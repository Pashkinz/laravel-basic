<?php

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/dev', 'DevController@index')->name('dev');
Route::middleware(['auth'])->group(function () {
    Route::resource('order', 'DeliveryOrderController');
    Route::resource('article', 'ArticleController');
    Route::resource('category', 'CategoryController');
    Route::resource('client', 'ClientController');
    Route::resource('client', 'ClientController');
    Route::resource('product', 'ProductController');
    Route::resource('promotional', 'PromotionalController');
    Route::resource('setting', 'SettingController');
    Route::resource('street', 'StreetController');
    Route::resource('user', 'User\UserController');
    Route::resource('permission', 'User\PermissionController');
    Route::resource('role', 'User\RoleController');
    Route::post('street/set_has_delivery', 'StreetController@setHasDelivery')
        ->name('street.set_has_delivery');
    Route::post('delete-media', 'MediaController@deleteMedia')->name('deleteMedia');

});
