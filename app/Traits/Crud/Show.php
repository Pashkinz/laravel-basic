<?php


namespace App\Traits\Crud;

trait Show
{

    protected $options = [];

    /**
     * @param string $key
     * @param string $value
     * @param string $type
     */
    function addDl($key = '', $value = '', $type = '')
    {
        $this->options[$key] = [
            'value' => $value,
            'type' => $type
        ];
    }

    function getPanel($title = '')
    {
        $panel = '';
        foreach ($this->options as $label => $data) {
            if ($data) {
                $panel .= view('inc.display.dl', ['label' => l($label),
                    'value' => $data['value'],
                    'type' => $data['type']])->render();
            }

        }
        $this->options = [];
        return view('inc.display.panel', compact('title', 'panel'))->render();
    }


}
