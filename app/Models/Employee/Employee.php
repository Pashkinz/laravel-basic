<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $guarded = ['id'];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'schedule_blank' => 'array',
    ];

    function main_specialty()
    {
        $specialties = $this->specialties()->where('is_main', 1)->get();
        if ($specialties) {
            $array = [];

            foreach ($specialties as $s) {
                $array[] = $s->specialty->name;
            }

            return implode(', ', $array);
        } else {
            return false;
        }
    }

    function professional_category()
    {
        return $this->belongsTo(\App\Models\Employee\ProfessionalCategory::class, 'professional_category_id');
    }

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function group()
    {
        return $this->belongsTo(EmployeeGroup::class, 'group_id');
    }

    public function specialties()
    {
        return $this->hasMany(EmployeeSpecialty::class, 'employee_id');
    }

    public function services()
    {
        return $this->hasMany(EmployeeService::class, 'employee_id');
    }


    public function schedule()
    {
        return $this->hasMany(Schedule::class, 'employee_id');
    }


    public function scopeGroup($query, $group_id)
    {
        if ($group_id == 0) {
            return;
        }

        $groups = EmployeeGroup::descendantsAndSelf($group_id)->toFlatTree()->pluck('id')->toArray();

        return $query->whereIn('group_id', $groups);

    }


}
