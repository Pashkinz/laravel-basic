<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;


class EmployeeGroup extends Model
{
    use  NodeTrait;

    protected $guarded = ['id'];

    static function getTree()
    {
        $array[0] = 'Корневой раздел';
        $traverse = function ($categories, $prefix = '-') use (&$traverse, &$array) {
            foreach ($categories as $category) {
                $array[$category->id] = $prefix . ' ' . $category->name;
                $traverse($category->children, $prefix . '-');
            }
        };

        $traverse(parent::get()->toTree());

        return $array;
    }

}
