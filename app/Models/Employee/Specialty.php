<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    protected $table = 'specialties';

    protected $guarded = ['id'];

    function group()
    {
        return $this->belongsTo(SpecialtyGroup::class, 'group_id');
    }

    public function scopeGroup($query, $group_id)
    {
        if ($group_id == 0) {
            return;
        }
        $groups = SpecialtyGroup::descendantsAndSelf($group_id)->toFlatTree()->pluck('id')->toArray();
        return $query->whereIn('group_id', $groups);

    }

    public function parent()
    {
        return $this->belongsTo($this, 'parent_id');
    }

}
