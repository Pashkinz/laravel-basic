<?php

namespace App\Models\Employee;

use App\Models\Service\Service;
use Illuminate\Database\Eloquent\Model;

class EmployeeService extends Model
{
    protected $table = 'employee_services';
    protected $guarded = ['id'];


    function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }


}
