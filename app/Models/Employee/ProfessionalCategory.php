<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class ProfessionalCategory extends Model
{
    protected $table = 'professional_categories';
    protected $guarded = ['id'];
}
