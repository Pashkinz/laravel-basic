<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class SpecialtyGroup extends Model
{
    use NodeTrait;

    protected $childrenArray = [];
    protected $guarded = ['id'];


    public function children()
    {
        return $this->hasMany(self::class, 'pid');
    }

    public function grandchildren()
    {
        return $this->children()->with('grandchildren');
    }

    function specialty()
    {
        return $this->hasMany(Specialty::class, 'group_id');
    }

    static function getTree()
    {
        $array[0] = 'Корневой раздел';
        $traverse = function ($categories, $prefix = '-') use (&$traverse, &$array) {
            foreach ($categories as $category) {
                $array[$category->id] = $prefix . ' ' . $category->name;
                $traverse($category->children, $prefix . '-');
            }
        };

        $traverse(parent::get()->toTree());

        return $array;
    }


    public function parent()
    {
        return $this->belongsTo($this, 'parent_id');
    }

}
