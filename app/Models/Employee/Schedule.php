<?php

namespace App\Models\Employee;

use App\Models\Workplace\Workplace;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedules_employees';
    protected $guarded = ['id'];

    protected $dates = [
        'date',
    ];

    function Employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    function workplace()
    {
        return $this->belongsTo(Workplace::class, 'workplace_id');
    }

    function getTimeFromAttribute($value)
    {
        if ($value != null) {
            $ar = explode(':', $value);

            return $ar[0] . ':' . $ar[1];
        }

        return $value;
    }

    function getTimeToAttribute($value)
    {
        if ($value != null) {
            $ar = explode(':', $value);

            return $ar[0] . ':' . $ar[1];
        }

        return $value;
    }
}
