<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class EmployeeSpecialty extends Model
{
    protected $table = 'employee_specialties';
    protected $guarded = ['id'];
    //receipt

    protected $dates = [
        'receipt',
        'last_courses'
    ];

    protected $casts = [
        'receipt' => 'datetime:Y-m-d',
        'last_courses' => 'datetime:Y-m-d',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function category()
    {
        return $this->belongsTo(ProfessionalCategory::class, 'category_id');
    }

    public function specialty()
    {
        return $this->belongsTo(Specialty::class, 'specialty_id');

    }


}
