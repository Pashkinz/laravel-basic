<?php

namespace App\Http\Controllers;

use App\Models\Street;
use Illuminate\Http\Request;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class StreetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $street = QueryBuilder::for(Street::class)
            ->allowedFilters(['name', 'id'])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('street.index', compact('street'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('street.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        street::create($request->except('_token'));

        return redirect()->route('street.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Street $street
     * @return \Illuminate\Http\Response
     */
    public function show(Street $street)
    {
        return view('street.show', compact('street'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Street $street
     * @return \Illuminate\Http\Response
     */
    public function edit(Street $street)
    {
        return view('street.edit', compact('street'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Street $street
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Street $street)
    {
        $this->validateRequest($request);
        $street->update($request->except('_token'));
        return redirect()->route('street.index');
    }

    /**
     * @param Street $street
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Street $street)
    {
        $street->delete();

        return redirect()->route('street.index');
    }

    /**
     * @param $request
     */
    public function validateRequest($request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setHasDelivery(Request $request)
    {
        $request->validate([
            'street_id' => 'required|integer',
        ]);

        $street = Street::findOrFail($request->get('street_id'));
        $has_delivery = ($street->has_delivery) ? 0 : 1;

        $street->update([
            'has_delivery' => $has_delivery
        ]);

        return response()->json([
            'status' => 'success',
        ]);

    }
}
