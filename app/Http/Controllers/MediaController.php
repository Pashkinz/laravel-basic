<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;

class MediaController extends Controller
{
    function deleteMedia(Request $request)
    {
        $request->validate([
            'media_id' => 'required|integer',
        ]);

        Media::findOrFail($request->get('media_id'))->delete();

        return response()->json([
            'status' => 'ok'
        ]);
    }
}
