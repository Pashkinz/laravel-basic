<?php

namespace App\Http\Controllers;

use App\Models\DeliveryOrderProduct;
use Illuminate\Http\Request;

class DeliveryOrderProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DeliveryOrderProduct  $deliveryOrderProduct
     * @return \Illuminate\Http\Response
     */
    public function show(DeliveryOrderProduct $deliveryOrderProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DeliveryOrderProduct  $deliveryOrderProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliveryOrderProduct $deliveryOrderProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DeliveryOrderProduct  $deliveryOrderProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliveryOrderProduct $deliveryOrderProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DeliveryOrderProduct  $deliveryOrderProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliveryOrderProduct $deliveryOrderProduct)
    {
        //
    }
}
