<?php

namespace App\Http\Controllers;

use App\Actions\Category\CreateCategoryAction;
use App\Actions\Category\UpdateCategoryAction;
use App\Http\Requests\Category\SaveCategoryRequest;
use App\Models\Category;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryController extends Controller
{
    /** @var CreateCategoryAction|\Illuminate\Contracts\Foundation\Application|mixed  */
    private $createCategoryAction;

    /**
     * @var UpdateCategoryAction|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $updateCategoryAction;

    public function __construct()
    {
        $this->createCategoryAction = app(CreateCategoryAction::class);
        $this->updateCategoryAction = app(UpdateCategoryAction::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $category = QueryBuilder::for(Category::class)
            ->allowedFilters(['name', 'id'])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('category.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveCategoryRequest $request)
    {
        $createCategoryResponse = $this->createCategoryAction->execute($request);

        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('category.edit', compact('category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $category
     * @return \Illuminate\Http\Response
     */
    public function update(SaveCategoryRequest $request)
    {
        $createReviewResponse = $this->updateCategoryAction->execute($request, $request->route()->parameter('category'));
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('category.index');
    }
}
