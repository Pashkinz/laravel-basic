<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission as Permission;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Illuminate\Validation\Rule;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = QueryBuilder::for(Permission::class)
            ->allowedFilters(['name', 'id'])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('user.permission.index', compact('permission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);
        Permission::create(['name' => $request->get('name')]);
        return redirect()->route('permission.index');
    }

    /**
     * @param Permission $permission
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Permission $permission)
    {
        return view('user.permission.show', compact('permission'));
    }

    /**
     * @param Permission $permission
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Permission $permission)
    {
        return view('user.permission.edit', compact('permission'));
    }

    /**
     * @param Request $request
     * @param Permission $permission
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Permission $permission)
    {

        \Validator::make($request->all(), [
            'name' => ['required|max:50|min:2|unique:permissions,name',
                Rule::unique('roles')->ignore($permission->id),
            ]
        ]);

        $permission->update($request->except('_token'));
        return redirect()->route('permission.index');
    }


    /**
     * @param $request
     */
    public function validateRequest($request)
    {
        $request->validate([
            'name' => 'required|max:50|min:2|unique:permissions',
        ]);

    }

    /**
     * @param Permission $permission
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Permission $permission)
    {
        $permission->delete();

        return redirect()->route('permission.index');
    }
}
