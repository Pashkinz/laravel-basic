<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role as Role;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = QueryBuilder::for(Role::class)
            ->allowedFilters(['name', 'id'])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('user.role.index', compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();
        return view('user.role.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50|min:2|unique:roles,name'
        ]);

        $role = Role::create($request->except('_token', 'permission'));

        if ($request->has('permission')) {
            $permission = $request->get('permission');
            $role->syncPermissions($permission);
        }

        return redirect()->route('role.index');
    }

    /**
     * @param Role $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Role $role)
    {
        $perm = ($role->permissions) ? implode(', ', $role->permissions->pluck('name')->toArray()) : '';

        return view('user.role.show', compact('role', 'perm'));
    }

    public function edit(Role $role)
    {
        if ($role->name == 'super-admin') {
            return abort(403);
        }
        $permissions = Permission::all();
        $role_perm = $role->permissions->pluck('name', 'id')->toArray();

        return view('user.role.edit', compact('role', 'permissions', 'role_perm'));
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Role $role)
    {
        \Validator::make($request->all(), [
            'name' => ['required|max:50|min:2|unique:roles,name',
                Rule::unique('roles')->ignore($role->id),
            ]
        ]);

        $role->update($request->except('_token', 'permission'));

        if ($request->has('permission')) {
            $permission = $request->get('permission');
            $role->syncPermissions($permission);
        } else {
            $role->syncPermissions([]);
        }

        return redirect()->route('role.index');
    }

    /**
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Role $role)
    {
        if ($role->name == 'super-admin') {
            return abort('403');
        }

        $role->delete();

        return redirect()->route('role.index');
    }


    /**
     * @param $request
     */
    public function validateRequest($request)
    {


    }

}
