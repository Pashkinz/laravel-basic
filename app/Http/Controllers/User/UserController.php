<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = QueryBuilder::for(User::class)
            ->allowedFilters(['name', 'id', 'email'])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('user.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $permissions = Permission::all();

        return view('user.user.create', compact('roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50|min:2',
            'email' => 'required|max:50|email|min:2|unique:users,email',
            'password' => 'required| string|min:8|confirmed',
        ]);

        $data = $request->all();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => \Hash::make($data['password']),
        ]);

        if ($request->has('roles')) {
            $roles = $request->get('roles');
            $user->syncRoles($roles);
        }

        if ($request->has('permissions')) {
            $user->syncPermissions($request->get('permissions'));
        }

        return redirect()->route('user.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        return view('user.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::all();
        $permissions = Permission::all();

        return view('user.user.edit', compact('user', 'roles', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        \Validator::make($request->all(), [
            'email' => ['required|max:50|min:2|unique:users,email|email',
                Rule::unique('roles')->ignore($user->id),
            ],
            'name' => ['required|max:50|min:2'],
        ]);

        $data['email'] = $request->get('email');
        $data['name'] = $request->get('name');

        if ($request->has('password') && $request->get('password') != '') {
            $request->validate([
                'password' => 'required|string|min:8|confirmed'
            ]);
            $data['password'] = \Hash::make($request->get('password'));
        }

        $user->update($data);

        if ($request->has('roles')) {
            $roles = $request->get('roles');
            $user->syncRoles($roles);
        }else{
            $user->syncRoles([]);

        }

        if ($request->has('permissions')) {
            $user->syncPermissions($request->get('permissions'));
        }else{
            $user->syncPermissions([]);

        }

        return redirect()->route('user.index');
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|void
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        if ($user->hasRole('super-admin')) {
            return abort(403);
        }

        $user->delete();

        return redirect()->route('user.index');
    }
}
