<?php

namespace App\Http\Controllers;

use App\Models\Promotional;
use Illuminate\Http\Request;


use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class PromotionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotional = QueryBuilder::for(Promotional::class)
            ->allowedFilters(['name', 'id'])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('promotional.index', compact('promotional'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promotional.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        promotional::create($request->except('_token'));

        return redirect()->route('promotional.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Promotional  $promotional
     * @return \Illuminate\Http\Response
     */
    public function show(Promotional $promotional)
    {
        return view('promotional.show', compact('promotional'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promotional  $promotional
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotional $promotional)
    {
        return view('promotional.edit', compact('promotional'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Promotional  $promotional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotional $promotional)
    {
        $this->validateRequest($request);
        $promotional->update($request->except('_token'));
        return redirect()->route('promotional.index');
    }

    /**
     * @param Promotional $promotional
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Promotional $promotional)
    {
        $promotional->delete();

        return redirect()->route('promotional.index');
    }

    /**
     * @param $request
     */
    public function validateRequest($request)
    {
        $request->validate([
            'name' => 'required|max:255',
        ]);

    }
}
