<?php

namespace App\Http\Controllers;

use App\Actions\Product\CreateProductAction;
use App\Http\Requests\Product\SaveProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class ProductController extends Controller
{
    /**
     * @var CreateProductAction|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $createProductAction;

    public function __construct()
    {
        $this->createProductAction = app(CreateProductAction::class);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        $product = QueryBuilder::for(Product::class)
            ->allowedFilters(['name', 'id', AllowedFilter::scope('category')])
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('product.index', compact('product', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();

        return view('product.create', compact('category'));
    }

    /**
     * @param SaveProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function store(SaveProductRequest $request)
    {
        $createProductResponse = $this->createProductAction->execute($request);
        /*$this->validateRequest($request);
        $product = Product::create($request->except('_token'));
        $this->addMediaAssets($request, $product);*/

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.show', compact('product'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $category = Category::all();
        return view('product.edit', compact('product', 'category'));

    }

    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function update(Request $request, Product $product)
    {
        $this->validateRequest($request);
        $product->update($request->except('_token', 'image'));
        $this->addMediaAssets($request, $product);
        return redirect()->route('product.index');
    }

    /**
     * @param Request $request
     * @param Product $product
     * @throws \Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted
     */
    public function addMediaAssets(Request $request, Product $product)
    {

        if ($request->has('image')) {
            if ($product->hasMedia()) {
                $product->deleteMedia($product->getFirstMedia()->id);
            }
            $imageName = md5(microtime()) . '.' . request()->image->getClientOriginalExtension();
            $product->addMediaFromRequest('image')->usingFileName($imageName)->toMediaCollection();
        }
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('product.index');
    }

    /**
     * @param $request
     */
    public function validateRequest($request)
    {
        $request->validate([
            'name' => 'required|max:255|min:2',
            'category_id' => 'required',
            'price' => 'required|numeric',
            'price_old' => 'numeric',
            'description' => 'max:1000',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

    }

}
