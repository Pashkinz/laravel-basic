<?php

namespace App\Http\Middleware;

use Closure;

class SetSettingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $settings = \DB::table('settings')->get();

        foreach ($settings as $setting) {
             \Config::set('app.settings.' . $setting->key, $setting->value);
        }

        return $next($request);
    }
}
