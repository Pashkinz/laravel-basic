<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Client;
use App\Models\Employee\Employee;
use App\Models\Employee\EmployeeService;
use App\Models\Employee\EmployeeSpecialty;
use App\Models\Employee\Schedule;
use App\Models\Employee\Specialty;
use App\Models\Enterprises\Affiliated\Company;
use App\Models\Product;
use App\Models\Promotional;
use App\Models\Street;
use App\User;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {

        parent::boot();

        Route::model('employee', Employee::class);

        Route::model('category', Category::class);
        Route::model('client', Client::class);
        Route::model('article', Article::class);
        Route::model('promotional', Promotional::class);
        Route::model('street', Street::class);
        Route::model('product', Product::class);
        Route::model('permission', Permission::class);
        Route::model('role', Role::class);
        Route::model('user', User::class);


    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));

        /*
          $controllersDir = [
              'Employee',
              'Service',
              'Workplace'
          ];

          foreach ($controllersDir as $dir) {

              Route::middleware('web')
                  ->namespace($this->namespace . '\\' . $dir)
                  ->prefix(mb_strtolower($dir))
                  ->group(base_path('app/Http/Controllers/' . $dir . '/web.php'));
          }


          $controllersDir = [];
           $ress = scandir(base_path('app/Http/Controllers/'));
           unset($ress[0]);
           unset($ress[1]);
           foreach ($ress as $res) {
               if (is_dir(base_path('app/Http/Controllers/') . $res)) {
                   if ($res != '.' || $res != '..') {
                       $controllersDir[] = $res;
                   }
               }
           }

           foreach ($controllersDir as $dir) {
               $pass = base_path('app/Http/Controllers/' . $dir . '/web.php');
               if (file_exists($pass)) {
                   Route::middleware('web')
                       ->namespace($this->namespace . '\\' . $dir)
                       ->prefix(mb_strtolower($dir))
                       ->group($pass);
               }

           }
    */
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
