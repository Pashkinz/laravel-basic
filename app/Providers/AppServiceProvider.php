<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Sven\ArtisanView\ServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::include('inc.actions.delete', 'delete');
        Blade::include('inc.actions.add', 'add');
        Blade::include('inc.actions.edit', 'edit');
        Blade::include('inc.display.boolean', 'boolean');
        Blade::include('inc.display.dl', 'dl');
        Blade::include('inc.display.image_media', 'image_media');

    }
}
