<?php

function get_filter_value($name = '')
{
    $request = \Request::all();
    if (isset($request['filter'][$name])) {
        return $request['filter'][$name];
    }
}
