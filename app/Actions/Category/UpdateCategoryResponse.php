<?php

namespace App\Actions\Category;

use App\Models\Category;

class UpdateCategoryResponse
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function getModel(): Category
    {
        return $this->category;
    }
}
