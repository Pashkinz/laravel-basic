<?php

namespace App\Actions\Category;

use App\Http\Requests\Category\SaveCategoryRequest;
use App\Models\Category;
use Illuminate\Support\Facades\Log;

class UpdateCategoryAction
{
    public function __construct() {

    }

    public function execute(SaveCategoryRequest $request, Category $category): UpdateCategoryResponse
    {
        $category->update(['name' => $request->input('name')]);

        Log::info("category: User {" . auth()->id() . "} added category {$category->id}");

        return new UpdateCategoryResponse($category);
    }

}
