<?php

namespace App\Actions\Category;

use App\Http\Requests\Category\SaveCategoryRequest;
use App\Models\Category;
use Illuminate\Support\Facades\Log;

class CreateCategoryAction
{
    public function __construct() {

    }

    public function execute(SaveCategoryRequest $request): CreateCategoryResponse
    {
        /** @var Category $category */
        $category = Category::create(['name' => $request->input('name')]);

        Log::info("category: User {" . auth()->id() . "} added category {$category->id}");

        return new CreateCategoryResponse($category);
    }

}
