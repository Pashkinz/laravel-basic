<?php

namespace App\Actions\Product;

use App\Models\Category;

class UpdateProductResponse
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function getModel(): Category
    {
        return $this->category;
    }
}
