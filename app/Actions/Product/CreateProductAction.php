<?php

namespace App\Actions\Product;

use App\Http\Requests\Product\SaveProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CreateProductAction
{
    use MediaTrait;

    public function __construct() {

    }

    public function execute(SaveProductRequest $request): CreateProductResponse
    {
        /** @var Product $product */
        $product = Product::create([
            'name' => $request->input('name'),
            'category_id' => $request->input('category_id'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'price_old' => $request->input('price_old'),
            'is_active' => $request->input('is_active'),
        ]);

        $this->addMediaAssets($request, $product);

        Log::info("product: User {" . auth()->id() . "} added product {$product->id}");

        return new CreateProductResponse($product);
    }

}
