<?php


namespace App\Actions\Product;


use App\Models\Product;
use Illuminate\Http\Request;

trait MediaTrait
{
    protected function addMediaAssets(Request $request, Product $product)
    {

        if ($request->has('image')) {
            if ($product->hasMedia()) {
                $product->deleteMedia($product->getFirstMedia()->id);
            }
            $imageName = md5(microtime()) . '.' . $request->image->getClientOriginalExtension();
            $product->addMediaFromRequest('image')->usingFileName($imageName)->toMediaCollection();
        }
    }
}
