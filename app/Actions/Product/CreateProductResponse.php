<?php

namespace App\Actions\Product;

use App\Models\Product;

class CreateProductResponse
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getModel(): Product
    {
        return $this->product;
    }
}
