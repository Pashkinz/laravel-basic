<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Данні для входу не правильні.',
    'throttle' => 'Перевищено кількість спроб для входу в систему. Зачекайте :seconds секунд.',
    'Logout' => 'Вихід'

];
