@extends('layouts.app')

@section('title')
    Акция
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                Наименование
            </dt>
            <dd class="col-sm-10">
                {{$promotional->name}}
            </dd>
        </dl>


        <dl class="row">
            <dt class="col-sm-2">
                Описание
            </dt>
            <dd class="col-sm-10">
                {!!  $promotional->description !!}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Активная
            </dt>
            <dd class="col-sm-10">
                @if($promotional->active)
                    <span class="badge badge-primary">Да</span>
                @else
                    <span class="badge badge-secondary">Нет</span>
                @endif
            </dd>
        </dl>
    </div>
@endsection

@section('actions')
    @edit(['route' => route('promotional.edit',['promotional' => $promotional->id])])&nbsp;
    @delete(['route' => route('promotional.destroy',['promotional' => $promotional->id])])
@endsection
