@extends('layouts.app')

@section('title')
    Создание    категории
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->route('category.store')->post() !!}
        {!! Form::text('name', 'Наименование') !!}
        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection
