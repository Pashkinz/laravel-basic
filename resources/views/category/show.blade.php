@extends('layouts.app')

@section('title')
    Категории
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                Наименование
            </dt>
            <dd class="col-sm-10">
                {{$category->name}}
            </dd>
        </dl>
    </div>
@endsection

@section('actions')
    @edit(['route' => route('category.edit',['category' => $category->id])])&nbsp;
    @delete(['route' => route('category.destroy',['category' => $category->id])])
@endsection
