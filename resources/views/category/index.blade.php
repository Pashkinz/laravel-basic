@extends('layouts.app')

@section('title')
    Категории
@endsection

@section('content')
    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Наименование</th>
                <th scope="col">Дата обновления</th>
                <th scope="col">Дата создания</th>
            </tr>
            </thead>
            <tbody>
            @foreach($category as $c)
                <tr url="{{route('category.show',['category' => $c->id])}}">
                    <td>{{$c->id}}</td>
                    <td>{{$c->name}}</td>
                    <td>{{$c->updated_at->format('H:i m.d.Y')}}</td>
                    <td>{{$c->created_at->format('H:i m.d.Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $category->links() }}
    </div>
@endsection


@section('actions')
    <div class="btn-toolbar mb-2 mb-md-0">
        @add(['route' => route('category.create')])
    </div>
@endsection

@push('css')

@endpush
