<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/components-font-awesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    @stack('css')
    @yield('after_css')
</head>
<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/">Sushi Wok Out</a>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">

            <a class="nav-link" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                @include('inc/menu')
            </div>
        </nav>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-1">
                @hasSection('title')
                    <h4>
                        @yield('title')
                    </h4>
                @endif
                @hasSection('actions')
                    <div class="btn-toolbar mb-2 mb-md-0">
                        @yield('actions')
                    </div>
                @endif
            </div>
            @yield('content')
        </main>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script>
    $(function () {
        $('body').on('dblclick', 'tr[url]', function () {
            window.location.href = $(this).attr('url');
        });
    });

    function deleteMedia(url, media_id, selector) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {media_id: media_id, _token: '{{csrf_token()}}'},
            success: function (data) {
                if (data.status === 'ok') {
                    $(selector).empty();
                }
            }
        });
    }
</script>

@stack('scripts')
@yield('after_scripts')
</body>
</html>
