@extends('layouts.app')

@section('title')
    Продукт
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                Наименование
            </dt>
            <dd class="col-sm-10">
                {{$product->name}}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Категория
            </dt>
            <dd class="col-sm-10">
                {{$product->category->name}}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Описание
            </dt>
            <dd class="col-sm-10">
                {!!  $product->description !!}
            </dd>
        </dl>


        <dl class="row">
            <dt class="col-sm-2">
                Цена
            </dt>
            <dd class="col-sm-10">
                {{$product->price}}
            </dd>
        </dl>


        <dl class="row">
            <dt class="col-sm-2">
                Старая цена
            </dt>
            <dd class="col-sm-10">
                {{$product->price_old}}
            </dd>
        </dl>


        <dl class="row">
            <dt class="col-sm-2">
                Изображение
            </dt>
            <dd class="col-sm-10">
                @image_media([ 'model' => $product, 'conversionName' => 'thumb'])
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Активный
            </dt>
            <dd class="col-sm-10">
                @if($product->is_active)
                    <span class="label label-success">Да</span>
                @else
                    <span class="label label-default">Нет</span>
                @endif
            </dd>
        </dl>


    </div>
@endsection

@section('actions')
    @edit(['route' => route('product.edit',['product' => $product->id])])&nbsp;
    @delete(['route' => route('product.destroy',['product' => $product->id])])
@endsection
