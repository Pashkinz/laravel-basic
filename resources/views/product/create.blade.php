@extends('layouts.app')

@section('title')
    Создание продукта
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('product.store')->post() !!}
        {!! Form::text('name', 'Наименование') !!}
        {!! Form::select('category_id','Категория',$category->pluck('name','id')) !!}
        {!! Form::textarea('description', 'Описание')->id( 'ckeditor') !!}
        {!! Form::text('price', 'Цена') !!}
        {!! Form::text('price_old', 'Старая цена') !!}
        {!! Form::file('image','Изображение') !!}
        {!! Form::select('is_active', 'Активный',[0 => 'Нет', 1 => 'Да']) !!}
        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection


@push('scripts')
    @include('inc.ckeditor')
@endpush

