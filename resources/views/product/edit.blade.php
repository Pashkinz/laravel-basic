@extends('layouts.app')

@section('title')
    Редактирование продукта
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->multipart()->route('product.update', ['product' => $product->id])->put()->fill($product) !!}

        {!! Form::text('name', 'Наименование') !!}
        {!! Form::select('category_id','Категория',$category->pluck('name','id')) !!}
        {!! Form::textarea('description', 'Описание')->id( 'ckeditor') !!}
        {!! Form::text('price', 'Цена') !!}
        {!! Form::text('price_old', 'Старая цена') !!}
        {!! Form::select('is_active', 'Активный',[0 => 'Нет', 1 => 'Да']) !!}
        {!! Form::file('image','Изображение'); !!}

        @image_media([ 'model' => $product, 'conversionName' => 'thumb','form' => true])

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection

@push('scripts')
    @include('inc.ckeditor')
@endpush

