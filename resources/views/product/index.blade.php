@extends('layouts.app')

@section('title')
    Продукты
@endsection

@section('content')

    <form action="" class="grid-filter" method="get">
        <div class="form-row">

            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i></button>
            </div>

            <div class="col-auto">
                <input class="form-control form-control-sm" type="text" value="{{get_filter_value('id')}}"
                       name="filter[id]" placeholder="Id">
            </div>

            <div class="col-auto">
                <input class="form-control form-control-sm" type="text" value="{{get_filter_value('name')}}"
                       name="filter[name]" placeholder="Наименование">
            </div>

            <div class="col-auto">
                <select class="form-control form-control-sm" name="filter[category]" id="">
                    <option value="">Категория</option>
                @foreach($category as $cat)
                        <option @if(\Request::has('filter.category') && \Request::get('filter')['category'] == $cat->id) selected
                                @endif  value="{{$cat->id}}">
                            {{$cat->name}}
                        </option>
                    @endforeach
                </select>
            </div>

        </div>
    </form>

    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Наименование</th>
                <th scope="col">Цена</th>
                <th scope="col">Старая цена</th>
                <th scope="col">Категория</th>
                <th scope="col">Активен</th>
                <th scope="col">Дата обновления</th>
                <th scope="col">Дата создания</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product as $a)
                <tr url="{{route('product.show',['product' => $a->id])}}">
                    <td>{{$a->id}}</td>
                    <td>{{$a->name}}</td>
                    <td>{{$a->price}}</td>
                    <td>{{$a->price_old}}</td>
                    <td>{{$a->category->name}}</td>
                    <td>
                        @if($a->is_active)
                            <span class="label label-success">Да</span>
                        @else
                            <span class="label label-default">Нет</span>
                        @endif
                    </td>
                    <td>{{$a->updated_at->format('H:i m.d.Y')}}</td>
                    <td>{{$a->created_at->format('H:i m.d.Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $product->links() }}
    </div>
@endsection

@section('actions')
    <div class="btn-toolbar mb-2 mb-md-0">
        @add(['route' => route('product.create')])
    </div>
@endsection

@push('css')

@endpush
