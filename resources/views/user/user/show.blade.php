@extends('layouts.app')

@section('title')
    Роль
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                Имя
            </dt>
            <dd class="col-sm-10">
                {{$user->name}}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Email
            </dt>
            <dd class="col-sm-10">
                {{$user->email}}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Роли
            </dt>
            <dd class="col-sm-10">
                {{($user->roles) ? implode(', ', $user->roles->pluck('name')->toArray()) : ''}}
            </dd>
        </dl>

        @if(!$user->hasRole('super-admin'))
            <dl class="row">
                <dt class="col-sm-2">
                    Разрешения
                </dt>
                <dd class="col-sm-10">
                    {{($user->getDirectPermissions()) ? implode(', ', $user->getDirectPermissions()->pluck('name')->toArray()) : ''}}
                </dd>
            </dl>
        @endif

    </div>
@endsection

@section('actions')
    @edit(['route' => route('user.edit',['user' => $user->id])])&nbsp;
    @delete(['route' => route('user.destroy',['user' => $user->id])])
@endsection
