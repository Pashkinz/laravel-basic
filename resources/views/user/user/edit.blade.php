@extends('layouts.app')

@section('title')
    Редактирование роли
@endsection

@section('content')
    <div class="content-m-t">

        {!! Form::open()->route('user.update', ['user' => $user->id])->put()->fill($user) !!}
        {!! Form::text('name', 'Имя') !!}
        {!! Form::text('email', 'Email') !!}

        {!! Form::text('password', 'Пароль') !!}
        {!! Form::text('password_confirmation', 'Повтор пароля') !!}


        <div class="form-group row">
            <label for="role" class="col-sm-2 col-form-label">
                Роли
            </label>

            <div class="col-sm-6">
                <div class="row">
                    @foreach($roles as $role)
                        <div class="col-sm-12">
                            <input type="checkbox" id="role-{{$role->id}}"
                                   @if($user->hasRole($role->name))
                                   checked
                                   @endif
                                   name="roles[]" value="{{$role->name}}">
                            <label for="role-{{$role->id}}">{{$role->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="permission" class="col-sm-2 col-form-label">Разрешения</label>
            <div class="col-sm-6">
                <div class="row">
                    @foreach($permissions as $permission)
                        <div class="col-sm-3">
                            <input type="checkbox" id="permission-{{$permission->id}}"
                                   @if($user->hasPermissionTo($permission->name))
                                   checked
                                   @endif
                                   name="permissions[]" value="{{$permission->name}}">
                            <label for="permission-{{$permission->id}}">{{$permission->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection

