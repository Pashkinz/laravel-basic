@extends('layouts.app')

@section('title')
    Пользователи
@endsection

@section('content')
    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Имя</th>
                <th scope="col">Email</th>
                <th scope="col">Роли</th>
                <th scope="col">Разрешения</th>
                <th scope="col">Дата обновления</th>
                <th scope="col">Дата создания</th>
            </tr>
            </thead>
            <tbody>
            @foreach($user as $c)
                <tr url="{{route('user.show',['user' => $c->id])}}">
                    <td>{{$c->id}}</td>
                    <td>{{$c->name}}</td>
                    <td>{{$c->email}}</td>
                    <td>
                        {{($c->roles) ? implode(', ', $c->roles->pluck('name')->toArray()) : ''}}
                    </td>
                    <td>
                        @if(!$c->hasRole('super-admin'))
                            {{($c->getDirectPermissions()) ? implode(', ', $c->getDirectPermissions()->pluck('name')->toArray()) : ''}}
                        @endif
                    </td>
                    <td>{{$c->updated_at->format('H:i m.d.Y')}}</td>
                    <td>{{$c->created_at->format('H:i m.d.Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $user->links() }}
    </div>
@endsection


@section('actions')
    <div class="btn-toolbar mb-2 mb-md-0">
        @add(['route' => route('user.create')])
    </div>
@endsection

@push('css')

@endpush
