@extends('layouts.app')

@section('title')
    Создание  пользователя
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->route('user.store')->post() !!}
        {!! Form::text('name', 'Имя') !!}
        {!! Form::text('email', 'Email') !!}

        {!! Form::text('password', 'Пароль') !!}
        {!! Form::text('password_confirmation', 'Повтор пароля') !!}


        <div class="form-group row">
            <label for="role" class="col-sm-2 col-form-label">
                Роли
            </label>

            <div class="col-sm-6">
                <div class="row">
                    @foreach($roles as $role)
                        <div class="col-sm-12">
                            <input type="checkbox" id="role-{{$role->id}}"
                                   name="roles[]" value="{{$role->name}}">
                            <label for="role-{{$role->id}}">{{$role->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


        <div class="form-group row">
            <label for="permission" class="col-sm-2 col-form-label">Доп. разрешения</label>
            <div class="col-sm-6">
                <div class="row">
                    @foreach($permissions as $permission)
                        <div class="col-sm-3">
                            <input type="checkbox" id="permission-{{$permission->id}}"
                                   name="permissions[]" value="{{$permission->name}}">
                            <label for="permission-{{$permission->id}}">{{$permission->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection
