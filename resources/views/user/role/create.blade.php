@extends('layouts.app')

@section('title')
    Создание  роли
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->route('role.store')->post() !!}
        {!! Form::text('name', 'Наименование') !!}

        <div class="form-group row">
            <label for="permission" class="col-sm-2 col-form-label">Разрешения</label>
            <div class="col-sm-6">
                <div class="row">
                    @foreach($permissions as $permission)
                        <div class="col-sm-3">
                            <input type="checkbox" id="permission-{{$permission->id}}"
                                   name="permission[]" value="{{$permission->id}}">
                            <label for="permission-{{$permission->id}}">{{$permission->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection
