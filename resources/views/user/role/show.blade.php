@extends('layouts.app')

@section('title')
    Роль
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                Наименование
            </dt>
            <dd class="col-sm-10">
                {{$role->name}}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Разрешения
            </dt>
            <dd class="col-sm-10">
                @if($role->name == 'super-admin')
                  <b>Как у Бога</b>
                @else
                    {{$perm}}
                @endif
            </dd>
        </dl>

    </div>
@endsection

@section('actions')
    @edit(['route' => route('role.edit',['role' => $role->id])])&nbsp;
    @delete(['route' => route('role.destroy',['role' => $role->id])])
@endsection
