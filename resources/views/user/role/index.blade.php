@extends('layouts.app')

@section('title')
    Роли
@endsection

@section('content')
    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Наименование</th>
                <th scope="col">Разрешения</th>
                <th scope="col">Дата обновления</th>
                <th scope="col">Дата создания</th>
            </tr>
            </thead>
            <tbody>
            @foreach($role as $c)
                <tr url="{{route('role.show',['role' => $c->id])}}">
                    <td>{{$c->id}}</td>
                    <td>{{$c->name}}</td>
                    <td>
                        @if($c->name == 'super-admin')
                            <b>Как у Бога</b>
                        @else
                            {{($c->permissions) ? implode(', ', $c->permissions->pluck('name')->toArray()) : ''}}
                        @endif
                    </td>
                    <td>{{$c->updated_at->format('H:i m.d.Y')}}</td>
                    <td>{{$c->created_at->format('H:i m.d.Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $role->links() }}
    </div>
@endsection


@section('actions')
    <div class="btn-toolbar mb-2 mb-md-0">
        @add(['route' => route('role.create')])
    </div>
@endsection

@push('css')

@endpush
