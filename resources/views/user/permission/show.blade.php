@extends('layouts.app')

@section('title')
    Разрешение
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                Наименование
            </dt>
            <dd class="col-sm-10">
                {{$permission->name}}
            </dd>
        </dl>
    </div>
@endsection

@section('actions')
    @edit(['route' => route('permission.edit',['permission' => $permission->id])])&nbsp;
    @delete(['route' => route('permission.destroy',['permission' => $permission->id])])
@endsection
