@extends('layouts.app')

@section('title')
    Создание    клинета
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->route('client.store')->post() !!}
        {!! Form::text('name', 'ФИО') !!}
        {!! Form::text('phone', 'Номер телефона') !!}
        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection
