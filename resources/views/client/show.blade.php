@extends('layouts.app')

@section('title')
    Клиент
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                ФИО
            </dt>
            <dd class="col-sm-10">
                {{$client->name}}
            </dd>
        </dl>
        <dl class="row">
            <dt class="col-sm-2">
                Номер телефона
            </dt>
            <dd class="col-sm-10">
                {{$client->phone}}
            </dd>
        </dl>
    </div>
@endsection

@section('actions')
    @edit(['route' => route('client.edit',['client' => $client->id])])&nbsp;
    @delete(['route' => route('client.destroy',['client' => $client->id])])
@endsection
