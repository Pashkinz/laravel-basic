@extends('layouts.app')

@section('title')
    Настройки
@endsection

@section('content')
    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Описание</th>
                <th scope="col">Ключ</th>
                <th scope="col">Значение</th>
                <th scope="col">Дата обновления</th>
                <th scope="col">Дата создания</th>
            </tr>
            </thead>
            <tbody>
            @foreach($setting as $a)
                <tr url="{{route('setting.show',['setting' => $a->id])}}">
                    <td>{{$a->id}}</td>
                    <td>{{$a->description}}</td>
                    <td>app.settings.{{$a->key}}</td>
                    <td>{{$a->value}}</td>
                    <td>{{$a->updated_at->format('H:i m.d.Y')}}</td>
                    <td>{{$a->created_at->format('H:i m.d.Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $setting->links() }}
    </div>

@endsection

@section('actions')
    <div class="btn-toolbar mb-2 mb-md-0">
        @add(['route' => route('setting.create')])
    </div>
@endsection

@push('css')

@endpush
