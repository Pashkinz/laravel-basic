@extends('layouts.app')

@section('title')
    Создание строки настройки
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->route('setting.store')->post() !!}
        {!! Form::text('description', 'Описание') !!}
        {!! Form::text('key', 'Ключ') !!}
        {!! Form::text('value', 'Значение') !!}
        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection
