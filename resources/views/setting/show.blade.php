@extends('layouts.app')

@section('title')
    Настройка
@endsection

@section('content')
    <div class="show-model">

        <dl class="row">
            <dt class="col-sm-2">
                Описание
            </dt>
            <dd class="col-sm-10">
                {{$setting->description}}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Ключ
            </dt>
            <dd class="col-sm-10">
                app.settings.{{$setting->key}}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Значение
            </dt>
            <dd class="col-sm-10">
                {{$setting->value}}
            </dd>
        </dl>

    </div>
@endsection

@section('actions')
    @edit(['route' => route('setting.edit',['setting' => $setting->id])])&nbsp;
    @delete(['route' => route('setting.destroy',['setting' => $setting->id])])
@endsection
