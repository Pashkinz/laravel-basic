@extends('layouts.app')

@section('title')
    Статьи
@endsection

@section('content')
    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Наименование</th>
                <th scope="col">Активна</th>
                <th scope="col">Дата обновления</th>
                <th scope="col">Дата создания</th>
            </tr>
            </thead>
            <tbody>
            @foreach($article as $a)
                <tr url="{{route('article.show',['article' => $a->id])}}">
                    <td>{{$a->id}}</td>
                    <td>{{$a->name}}</td>
                    <td>
                        @if($a->active)
                            <span class="badge badge-primary">Да</span>
                        @else
                            <span class="badge badge-secondary">Нет</span>
                        @endif
                    </td>
                    <td>{{$a->updated_at->format('H:i m.d.Y')}}</td>
                    <td>{{$a->created_at->format('H:i m.d.Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $article->links() }}
    </div>
@endsection

@section('actions')
    <div class="btn-toolbar mb-2 mb-md-0">
        @add(['route' => route('article.create')])
    </div>
@endsection

@push('css')

@endpush
