@extends('layouts.app')

@section('title')
    Статья
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                Наименование
            </dt>
            <dd class="col-sm-10">
                {{$article->name}}
            </dd>
        </dl>


        <dl class="row">
            <dt class="col-sm-2">
                Текст
            </dt>
            <dd class="col-sm-10">
                {!!  $article->text !!}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Активная
            </dt>
            <dd class="col-sm-10">
                @if($article->active)
                    <span class="badge badge-primary">Да</span>
                @else
                    <span class="badge badge-secondary">Нет</span>
                @endif
            </dd>
        </dl>

    </div>
@endsection

@section('actions')
    @edit(['route' => route('article.edit',['article' => $article->id])])&nbsp;
    @delete(['route' => route('article.destroy',['article' => $article->id])])
@endsection
