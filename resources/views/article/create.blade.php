@extends('layouts.app')

@section('title')
    Создание статьи
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->route('article.store')->post() !!}
        {!! Form::text('name', 'Наименование') !!}
        {!! Form::textarea('text', 'Текст')->id( 'ckeditor') !!}
        {!! Form::file('image','Изображение') !!}
        {!! Form::select('active', 'Активный',[0 => 'Нет', 1 => 'Да']) !!}
        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection


@push('scripts')
    @include('inc.ckeditor')
@endpush

