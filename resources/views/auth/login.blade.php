@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <article class="card-body">
                        <a href="{{route('register')}}" class="float-right btn btn-outline-primary">{{ __('Register') }}</a>
                        <h4 class="card-title mb-4 mt-1">{{ __('Login') }}</h4>
                        <form method="POST" {{ route('login') }}>
                            @csrf

                            <div class="form-group">
                                <label>{{ __('Email') }}</label>
                                <input id="email" type="email"
                                       class="form-control @error('email') is-invalid @enderror" name="email"
                                       value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> <!-- form-group// -->

                            <div class="form-group">
                                @if (Route::has('password.request'))
                                    <a class="float-right" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif

                                <label>{{ __('Password') }}</label>
                                <input id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> <!-- form-group// -->

                            <div class="form-group">
                                <div class="checkbox">
                                    <label><input   type="checkbox" name="remember"
                                                  id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{ __('Remember Me') }}</label>
                                </div> <!-- checkbox .// -->
                            </div> <!-- form-group// -->

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Login') }}
                                </button>
                            </div> <!-- form-group// -->
                        </form>
                    </article>
                </div>
            </div>
        </div>
    </div>
@endsection
