@extends('layouts.app')

@section('title')
    Улица
@endsection

@section('content')
    <div class="show-model">
        <dl class="row">
            <dt class="col-sm-2">
                Наименование
            </dt>
            <dd class="col-sm-10">
                {{$street->name}}
            </dd>
        </dl>

        <dl class="row">
            <dt class="col-sm-2">
                Доставка
            </dt>
            <dd class="col-sm-10">
                @if($street->has_delivery)
                    <span class="badge badge-primary">Да</span>
                @else
                    <span class="badge badge-secondary">Нет</span>
                @endif
            </dd>
        </dl>

    </div>
@endsection

@section('actions')
    @edit(['route' => route('street.edit',['street' => $street->id])])&nbsp;
    @delete(['route' => route('street.destroy',['street' => $street->id])])
@endsection
