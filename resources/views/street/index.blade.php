@extends('layouts.app')

@section('title')
    Улицы
@endsection

@section('content')
    <div class="table-responsive">
        <table id="grid-table" class="table  table-hover table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Наименование</th>
                <th scope="col">Доставка</th>
                <th scope="col">Дата обновления</th>
                <th scope="col">Дата создания</th>
            </tr>
            </thead>
            <tbody>
            @foreach($street as $a)
                <tr url="{{route('street.show',['street' => $a->id])}}">
                    <td>{{$a->id}}</td>
                    <td>{{$a->name}}</td>
                    <td>
                        <input class="has_delivery" type="checkbox" @if($a->has_delivery) checked @endif
                        value="{{$a->id}}">
                    </td>
                    <td>{{$a->updated_at->format('H:i m.d.Y')}}</td>
                    <td>{{$a->created_at->format('H:i m.d.Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $street->links() }}
    </div>
@endsection

@section('actions')
    <div class="btn-toolbar mb-2 mb-md-0">
        @add(['route' => route('street.create')])
    </div>
@endsection

@push('css')

@endpush

@push('scripts')
    <script>
        $(function () {
            $('#grid-table .has_delivery').on('change', function (e) {
                $.ajax({
                    data: {
                        'street_id': $(this).attr('value'),
                        '_token': '{{csrf_token()}}'
                    },
                    url: '{{route('street.set_has_delivery')}}',
                    type: 'POST',
                    dataType: 'JSON'
                });
                e.preventDefault();
            });
        });
    </script>
@endpush

