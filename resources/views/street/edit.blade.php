@extends('layouts.app')

@section('title')
    Редактирование улицы
@endsection

@section('content')
    <div class="content-m-t">
        {!! Form::open()->route('street.update', ['street' => $street->id])->put()->fill($street) !!}
        {!! Form::text('name', 'Наименование') !!}
        {!! Form::select('has_delivery', 'Доставка',[0 => 'Нет', 1 => 'Да']) !!}
        <div class="form-group row">
            <div class="offset-2 col-sm-10">
                {!! Form::submit('Сохранить изменения') !!}
            </div>
        </div>
        {!!  Form::close() !!}
    </div>
@endsection

@section('actions')

@endsection

