
<form action="{{ $route}}" method="POST" style="display: inline">
    <input name="_method" type="hidden" value="DELETE">
    {{ csrf_field() }}

    <button style="margin-left: 5px" type="submit" class="btn btn-sm  {{ $btn ?? '' }} btn-danger">  <i class="fa fa-trash"></i>  {{'Удалить'}}</button>
</form>
