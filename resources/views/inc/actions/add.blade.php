<a class="btn {{ $btn ?? '' }} btn-sm btn-primary" href="{{$route}}"> <i class="fa fa-plus-circle"></i>
    @if(isset($text))
        {{$text}}
    @else
        Создать
    @endif
</a>
