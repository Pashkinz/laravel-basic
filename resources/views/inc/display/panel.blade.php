<fieldset>
    @if($title != '')
        <h6 style="margin-bottom: 10px; margin-top: 20px">{{l($title)}}</h6>
        {!! $panel !!}
    @else
        <div style="margin-top: 25px">
            {!! $panel !!}
        </div>
    @endif
</fieldset>


