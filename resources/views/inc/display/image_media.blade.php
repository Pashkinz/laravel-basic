@if(isset($model) && $model->hasMedia())
        @if(isset($form))
            <div class="form-group row">
                <div class="offset-2 col-sm-10">
                    @endif
                    <div id="media-image">
                        <img src="{{asset($model->getFirstMedia()->getUrl($conversionName))}}" alt="">
                        <br>
                        <a onclick="deleteMedia('{{route('deleteMedia')}}',{{$model->getFirstMedia()->id}}, '#media-image'); return false;"
                           href="#"
                           class="btn btn-danger btn-sm">Удалить</a>
                    </div>
                    @if(isset($form))

                </div>
            </div>
        @endif
@endif

