<ul class="nav flex-column">

    <li class="nav-item ">
        <a class="nav-link  @if(!\Request::segment(1)) active @endif" href="{{route('home')}}">
            <span class="fa fa-home"></span>
            Главная панель
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{route('order.index')}}">
            <span class="fa fa-shopping-bag"></span>
            Заказы
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(\Request::segment(1) == 'client') active @endif" href="{{route('client.index')}}">
            <span class="fa fa-users"></span>
            Клиенты
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link  @if(\Request::segment(1) == 'product') active @endif" href="{{route('product.index')}}">
            <span class="fa fa-luggage-cart"></span>
            Товары
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link  @if(\Request::segment(1) == 'category') active @endif" href="{{route('category.index')}}">
            <span class="fa fa-th-list"></span>
            Категории
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link   @if(\Request::segment(1) == 'promotional') active @endif"
           href="{{route('promotional.index')}}">
            <span class="fa fa-percent"></span>
            Акции
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(\Request::segment(1) == 'article') active @endif" href="{{route('article.index')}}">
            <span class="fa fa-book"></span>
            Статьи
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link  @if(\Request::segment(1) == 'street') active @endif" href="{{route('street.index')}}">
            <span class="fa fa-street-view"></span>
            Улицы
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link   @if(\Request::segment(1) == 'setting') active @endif" href="{{route('setting.index')}}">
            <span class="fa fa-cogs"></span>
            Настройки
        </a>
    </li>

</ul>

<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
    <span>Пользователи</span>
</h6>

<ul class="nav flex-column mb-2">

    <li class="nav-item">
        <a class="nav-link @if(\Request::segment(1) == 'user') active @endif"
           href="{{route('user.index')}}">
            Пользователи
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(\Request::segment(1) == 'role') active @endif"
           href="{{route('role.index')}}">
            Роли
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(\Request::segment(1) == 'permission') active @endif"
           href="{{route('permission.index')}}">
            Разрешения
        </a>
    </li>

</ul>


{{-----
<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
    <span>Справочник</span>
</h6>

<ul class="nav flex-column mb-2">

    <li class="nav-item">
        <a class="nav-link "
           href="#">
           Предприятия
        </a>
    </li>
</ul>
------}}
