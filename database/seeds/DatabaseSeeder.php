<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EmployeeGroupsTableSeeder::class);
        $this->call(EmployeeServicesTableSeeder::class);
        $this->call(EmployeeSpecialtiesTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
        $this->call(OfficesTableSeeder::class);
        $this->call(PatientsTableSeeder::class);
        $this->call(ProfessionalCategoriesTableSeeder::class);
        $this->call(ServiceGroupsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(SpecialtiesTableSeeder::class);
        $this->call(SpecialtyGroupsTableSeeder::class);
        $this->call(TranslatesTableSeeder::class);
        $this->call(WorkplacesTableSeeder::class);
    }
}
